from Generator import Generator
from Tokenizer import Tokenizer
from pathlib import Path
import json
import torch

class Alophone2MFCC:
    def __init__(self, alophone_tokenizer:Path,
                    generator:Path):
        self.alophone_tokenizer = alophone_tokenizer
        self.max_len = 25
        self.generator = generator
    
    def __call__(self, word):
        word, mask =  self.alophone_tokenizer(word, for_rnn=False)

        mfcc_hat = self.generator(word[None, ...], mask[None, ...])

        mask = mask[None, :, 0]
        mfcc_hat = mfcc_hat[mask].detach().cpu().numpy()

        res = []
        for mfcc_vec in mfcc_hat:
          res.append(mfcc_vec.tolist())

        return res
    




