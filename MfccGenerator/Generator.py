from torch import nn
import torch.nn.functional as F
import torch

class Generator(nn.Module):
    def __init__(self, input_size=61, out_size=12):
        super(Generator, self).__init__()

        self.embedding = nn.Embedding(input_size, 100)
        self.attention_1 = nn.MultiheadAttention(100, 10, batch_first=True)
        self.ff_att_1    = nn.Linear(100, 100)
        self.attention_2 = nn.MultiheadAttention(100, 10, batch_first=True)
        self.ff_att_2    = nn.Linear(100, 100)
        self.attention_3 = nn.MultiheadAttention(100, 10, batch_first=True)
        self.ff_att_3    = nn.Linear(100, 100)
        self.lin_out = nn.Linear(100, out_size)
    
    def forward(self, x, attn_mask):
        attn_mask = torch.cat([attn_mask for _ in range(10)])
        x = self.embedding(x)
        x, _ = self.attention_1(x, x, x, attn_mask=attn_mask)
        x    = F.relu(self.ff_att_1(x))
        x, _ = self.attention_2(x, x, x, attn_mask=attn_mask)
        x    = F.relu(self.ff_att_2(x))
        x, _ = self.attention_3(x, x, x, attn_mask=attn_mask)
        x    = F.relu(self.ff_att_3(x))
        x = self.lin_out(x)
        x = torch.nan_to_num(x)
        return x
    
    @staticmethod
    def load(path, input_size=60, hidden_size=12):
        encoder = Generator(input_size, hidden_size)
        state_dict = torch.load(path)
        encoder.load_state_dict(state_dict)
        encoder.eval()
        return encoder
