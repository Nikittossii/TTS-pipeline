import argparse
from pathlib import Path
from Alophone2MFCC import Alophone2MFCC
from Tokenizer import Tokenizer
from Generator import Generator
from bs4 import BeautifulSoup as bs
import json
import logging
from tqdm.auto import tqdm

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("Word2Alophone")

def load_model():
    model = Alophone2MFCC(
        Tokenizer.load("MfccGenerator/models/alophone_tokenizer_for_GUN40Full.pkl"),
        Generator.load("MfccGenerator/models/generator_GUN40Full.pth"),
    )
    return model

def setup_argparse():
    parser = argparse.ArgumentParser(
                    prog = 'Word2Alophone',
                    description = 'Inference of seq2seq model for predicting alophones')
    parser.add_argument("--xml", type=str, help='Path to xml file', required=True)
    parser.add_argument("--out", type=str, help='Path to output json file', required=True)
    args = parser.parse_args()
    return args

def read_xml(xml_file):
    dataset = []
    for sentence in xml_file.find_all("sentence"):
        sentence_lst = []
        word = {}
        for child in sentence.children:
            if child.name == "word":
                if "content" in word:
                    sentence_lst.append(word)
                    word = {} 

                alo_array = []
                for alo in child.find_all("allophone"):
                    alo_array.append(alo["ph"])

                word["allophone"] = alo_array
                word["content"] = child["original"] if "original" in child.attrs else ""

        sentence_lst.append(word)
        dataset.append(sentence_lst)
    return dataset

def load_xml(path):
    with open(path, 'r') as fd:
        bs_content = bs(fd.read(), "lxml")
    return bs_content

def parse_xml(path):
    bs_content = load_xml(path)
    dataset = read_xml(bs_content)
    return dataset

def main():
    # log.info("Start reading xml file")
    args = setup_argparse()
    log.info("Converting xml to readable format")
    text = parse_xml(args.xml)
    log.info("Load Seq2Seq model")
    model = load_model()
    result = []
    log.info("Start processing")
    for sentence in tqdm(text):
        processed_sentence = []
        for word in sentence:
            tmp = {}
            mfcc = model(word["allophone"])
            tmp["content"] = word["content"]
            tmp["mfcc"] = mfcc
            processed_sentence.append(tmp)
        result.append({"words":processed_sentence})
    text = result
    log.info("Complete processing")
    with open(args.out, "w") as f:
        json.dump(text, f, indent=4, ensure_ascii=False)
    log.info(f"Result saved at {args.out}")



if __name__ == "__main__":
    main()
