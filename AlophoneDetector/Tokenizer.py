from typing import List
import torch
import pickle
import numpy as np

class Tokenizer:
    def __init__(self, sentences:List[List[str]], type_token="letter"):
        if sentences:
            max_len = max([ len(word[type_token])
                                for sentence in sentences 
                                for word in sentence])
            self.max_length = max_len + 2
            unique_char = set([char  
                                for sentence in sentences 
                                for word in sentence
                                for char in word[type_token]])
            
            unique_char = ["<PAD>", "<SOW>", "<EOW>"] + list(unique_char)

            self.dictionary = {char:idx for idx, char in enumerate(unique_char)} 
            self.decoder_dict = {idx:char for char, idx in self.dictionary.items()}

    def decode(self, token_seq, return_string=False):
        token_seq = token_seq.long().numpy()
        token_seq =  token_seq[token_seq > 2]
        if return_string:
            return " ".join([self.decoder_dict[idx] for idx in token_seq])
        else:
            return [self.decoder_dict[idx] for idx in token_seq]

    def __len__(self):
        return len(self.dictionary)

    def __call__(self, word:List[str], for_rnn=False):
        seq = [1] + [self.dictionary[x] for x in word] + [2]
        pad = [0 for _ in range(self.max_length - len(seq))]
        if for_rnn:
            seq = pad + seq
        else:
            seq = seq + pad 
        return torch.Tensor(seq).long()
    
    @staticmethod
    def load(path):
        with open(path, "rb") as f:
            tokenizer = pickle.load(f)
        return tokenizer
