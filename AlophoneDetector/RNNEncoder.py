from torch import nn
import torch.nn.functional as F
import torch

class EncoderRNN(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.lstm = nn.LSTM(hidden_size, hidden_size, bidirectional=True,
                            batch_first=True)
        self.layer_norm = nn.LayerNorm(hidden_size*2)
        self.fc = nn.Linear(hidden_size*2, hidden_size)
    def forward(self, input, hidden=None, c0=None):
        if not hidden:
          b_size = input.shape[0]
          hidden = self.initHidden(b_size)
          c0 = self.initHidden(b_size)

        embedded = self.embedding(input)
        output = embedded

        output, (hidden, c0) = self.lstm(output, (hidden, c0))
        output = self.layer_norm(output)
        output = F.relu(self.fc(output))
        return output, hidden, c0

    def initHidden(self, b_size=1):
        return torch.zeros(2, b_size, self.hidden_size)
    
    @staticmethod
    def load(path, input_size=37, hidden_size=256):
        encoder = EncoderRNN(input_size, hidden_size)
        state_dict = torch.load(path)
        encoder.load_state_dict(state_dict)
        encoder.eval()
        return encoder
