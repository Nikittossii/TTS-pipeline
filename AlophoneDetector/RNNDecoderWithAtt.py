from torch import nn
import torch.nn.functional as F
import torch

class AttnDecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size, max_length=25):
        super(AttnDecoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.max_length = max_length

        self.embedding = nn.Embedding(self.output_size, self.hidden_size)
        self.attn = nn.Linear(self.hidden_size * 2, self.max_length)
        self.attn_combine = nn.Linear(self.hidden_size * 2, self.hidden_size)
        self.lstm = nn.LSTM(self.hidden_size, self.hidden_size, 2, 
                            batch_first=True)
        self.out = nn.Linear(self.hidden_size, self.output_size)

    def forward(self, input, hidden, c0, encoder_outputs):
        embedded = self.embedding(input)
        
        attn_weights = F.softmax(
            self.attn(torch.cat((embedded, hidden[-1]), 1)), dim=1).unsqueeze(1)
        attn_applied = torch.bmm(attn_weights,
                                 encoder_outputs)

        output = torch.cat((embedded, attn_applied.squeeze(1)), 1)
        output = self.attn_combine(output).unsqueeze(1)

        output = F.relu(output)

        output, (hidden, c0) = self.lstm(output, (hidden, c0))

        output = F.log_softmax(self.out(output.squeeze(1)), dim=1)
        return output, hidden, c0, attn_weights

    def initHidden(self, batch_sz):
        return torch.zeros(2, batch_sz, self.hidden_size)
    
    @staticmethod
    def load(path, input_size=256, hidden_size=61):
        encoder = AttnDecoderRNN(input_size, hidden_size)
        state_dict = torch.load(path)
        encoder.load_state_dict(state_dict)
        encoder.eval()
        return encoder
