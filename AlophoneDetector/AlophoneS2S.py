from RNNDecoderWithAtt import AttnDecoderRNN
from RNNEncoder import EncoderRNN
from Tokenizer import Tokenizer
from pathlib import Path
import json
import torch

class AlophoneSeq2Seq:
    def __init__(self, word_tokenizer:Path, alophone_tokenizer:Path,
                    encoder:Path, decoder:Path):
        self.word_tokenizer = word_tokenizer
        self.alophone_tokenizer = alophone_tokenizer
        self.max_len = 25
        self.encoder = encoder
        self.decoder = decoder
    
    def __call__(self, word):
        word =  self.word_tokenizer(word, for_rnn=True)[None, ...]

        encoder_outputs, encoder_hidden, encoder_c0 = self.encoder(word)

        decoder_hidden = encoder_hidden
        decoder_c0 = encoder_c0

        decoder_input = torch.tensor([1])
        generated_outputs = [decoder_input]
        
        for _ in range(self.max_len):
          decoder_output, decoder_hidden, decoder_c0, _ = self.decoder(
                  decoder_input, decoder_hidden, decoder_c0, encoder_outputs)
          decoder_input = torch.argmax(decoder_output, dim=1)

          generated_outputs.append(decoder_input)
          if (decoder_input == 2).all():
            break
        generated_outputs = torch.stack(generated_outputs).squeeze()
        decoded_outputs = self.alophone_tokenizer.decode(generated_outputs)

        return decoded_outputs
    




