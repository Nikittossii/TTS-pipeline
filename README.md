# TTS-pipeline
TTS-ITMO course

* "Мастер и маргорита" in dvc storage
* Lab 1: Prosodic detector - Pause detection.ipynb
* Lab 2: Word2Alophone - LSTM Seq2Seq model with basic attention mechanism:
    * AlophoneDetector/Allophone_char_seq2seq.ipynb - trainer script
    * AlophoneDetector/main.py - inference 